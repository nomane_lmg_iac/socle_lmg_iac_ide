#!/bin/bash

echo "Activation du mode teletravail"


if [ -f ~/.ssh/config ]; then
    mv ~/.ssh/config ~/.ssh/config.deactivate
fi

alias sshiac="ssh pliacaza01"

# Supprime les proxy
unset https_proxy
unset http_proxy
unset no_proxy
