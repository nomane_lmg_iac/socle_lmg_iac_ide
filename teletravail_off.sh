#!/bin/bash

echo "Désactivation du mode télétravail"

if [ ! -f ~/.ssh/config ]; then
    mv ~/.ssh/config.deactivate ~/.ssh/config
fi

# Activation du proxy interne
export https_proxy='http://165.225.76.40:9400'
export http_proxy='http://165.225.76.40:9400'
export no_proxy='localhost,127.0.0.1,.mgsi.mg.com.fr'
